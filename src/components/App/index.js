import React, {Fragment} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Home from '../Home';

import Footer from './Footer';
import Header from './Header';

const App = () => (
    <BrowserRouter>
        <Fragment>
            <Header/>
            <main>
                <Switch>
                    <Route exact path="/" component={Home}/>
                </Switch>
            </main>
            <Footer/>
        </Fragment>
    </BrowserRouter>
);

export default App;
